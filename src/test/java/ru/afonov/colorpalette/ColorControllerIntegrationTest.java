package ru.afonov.colorpalette;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.afonov.colorpalette.client.ColorClient;
import ru.afonov.colorpalette.client.ColorDetails;
import ru.afonov.colorpalette.client.ColorName;
import ru.afonov.colorpalette.util.JwtUtility;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ColorControllerIntegrationTest extends BaseTestClass {

    private static String AUTHORIZATION_HEADER;

    @MockBean
    private ColorClient colorClient;

    @BeforeAll
    static void setUp() {
        String username = "ivan123";
        AUTHORIZATION_HEADER = JwtUtility.createJwtForUser(username);
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql"})
    void successfullyCreateColor() throws Exception {
        String paletteId = "1";
        String createRequest = "{ \"hex\": \"152EEA\", \"name\": \"Test Color\" }";

        String colorName = "Test color name";
        ColorName testColorName = new ColorName(colorName, "closestNamedHex", true, 0);
        ColorDetails testColorDetails = new ColorDetails(testColorName);
        Mockito.when(colorClient.getColorInfo("152EEA")).thenReturn(testColorDetails);


        ResultActions result = mockMvc.perform(
                post("/palettes/" + paletteId + "/colors")
                        .header("Authorization", AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(createRequest)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(colorName));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql", "classpath:db/add_colors.sql"})
    void successfullyGetColorsByPaletteId() throws Exception {
        String paletteId = "1";


        ResultActions result = mockMvc.perform(
                get("/palettes/" + paletteId + "/colors")
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql", "classpath:db/add_colors.sql"})
    void successfullyGetColorById() throws Exception {
        long paletteId = 1L;
        long colorId = 2L;

        ResultActions result = mockMvc.perform(
                get("/palettes/" + paletteId + "/colors/" + colorId)
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Cobalt"));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql", "classpath:db/add_colors.sql"})
    void successfullyUpdateColor() throws Exception {
        long paletteId = 1L;
        long colorId = 2L;
        String updateRequest = "{ \"hex\": \"152EEA\", \"name\": \"Test Color\" }";
        String colorName = "Test color name";
        ColorName testColorName = new ColorName(colorName, "closestNamedHex", true, 0);
        ColorDetails testColorDetails = new ColorDetails(testColorName);
        Mockito.when(colorClient.getColorInfo("152EEA")).thenReturn(testColorDetails);


        ResultActions result = mockMvc.perform(
                put("/palettes/" + paletteId + "/colors/" + colorId)
                        .header("Authorization", AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateRequest)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(colorName));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql", "classpath:db/add_colors.sql"})
    void successfullyDeleteColorById() throws Exception {
        long paletteId = 1L;
        long colorId = 1L;


        ResultActions result = mockMvc.perform(
                delete("/palettes/" + paletteId + "/colors/" + colorId)
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk());
    }

}
