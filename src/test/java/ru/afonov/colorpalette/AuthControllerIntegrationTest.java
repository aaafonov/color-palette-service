package ru.afonov.colorpalette;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AuthControllerIntegrationTest extends BaseTestClass {

    @Test
    void successfullyUserRegistration() throws Exception {
        String json = "{ \"name\": \"Ivan\", \"login\": \"ivan123\", \"password\": \"password\" }";

        mockMvc.perform(post("/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))


                .andExpect(status().isOk());
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql"})
    void successfulUserAuthentication() throws Exception {
        String json = "{ \"name\": \"Ivan\", \"login\": \"ivan123\", \"password\": \"password\" }";

        mockMvc.perform(post("/auth/login")
                        .contentType("application/json")
                        .content(json))


                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").exists());
    }
}
