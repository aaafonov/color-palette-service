package ru.afonov.colorpalette.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;

import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

public class JwtUtility {

    private final static String secretString = "bXlTdXBlclNlY3JldEtleS1teVN1cGVyU2VjcmV0S2V5LW15U3VwZXJTZWNyZXRLZXk=";

    public static String createJwtForUser(String username) {
        long tokenExpirationInMilliseconds = 120000;
        Date now = new Date();
        byte[] keyBytes = Decoders.BASE64.decode(secretString);
        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
        return "Bearer " + Jwts.builder()
                .subject(username)
                .issuedAt(now)
                .expiration(new Date(now.getTime() + tokenExpirationInMilliseconds))
                .signWith(keySpec)
                .compact();
    }
}
