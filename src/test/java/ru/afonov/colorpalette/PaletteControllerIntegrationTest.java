package ru.afonov.colorpalette;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.ResultActions;
import ru.afonov.colorpalette.util.JwtUtility;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class PaletteControllerIntegrationTest extends BaseTestClass{

    private static String AUTHORIZATION_HEADER;

    @BeforeAll
    static void setUp() {
        String username = "ivan123";
        AUTHORIZATION_HEADER = JwtUtility.createJwtForUser(username);
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql"})
    void successfullyCreatePalette() throws Exception {
        String paletteJson = "{ \"name\": \"Test Palette\" }";


        ResultActions result = mockMvc.perform(
                post("/palettes")
                        .header("Authorization", AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(paletteJson)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test Palette"));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql"})
    void successfullyGetPalettes() throws Exception {
        ResultActions result = mockMvc.perform(
                get("/palettes")
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql"})
    void successfullyGetPaletteById() throws Exception {
        long paletteId = 1L;


        ResultActions result = mockMvc.perform(
                get("/palettes/" + paletteId)
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test Palette 1"));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql"})
    void successfullyUpdatePalette() throws Exception {
        long paletteId = 2L;
        String updateJson = "{ \"name\": \"Updated Palette\" }";


        ResultActions result = mockMvc.perform(
                put("/palettes/" + paletteId)
                        .header("Authorization", AUTHORIZATION_HEADER)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateJson)
        );


        result.andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Updated Palette"));
    }

    @Test
    @Sql(scripts = {"classpath:db/add_user.sql", "classpath:db/add_palettes.sql"})
    void successfullyDeletePaletteById() throws Exception {
        long paletteId = 2L;


        ResultActions result = mockMvc.perform(
                delete("/palettes/" + paletteId)
                        .header("Authorization", AUTHORIZATION_HEADER)
        );


        result.andExpect(status().isOk());
    }
}
