package ru.afonov.colorpalette.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.afonov.colorpalette.model.Palette;

import java.util.List;
import java.util.Optional;

public interface PaletteRepository extends JpaRepository<Palette, Long> {

    List<Palette> findByUserId(Long userId);

    Optional<Palette> findByIdAndUserId(Long id, Long userId);
}
