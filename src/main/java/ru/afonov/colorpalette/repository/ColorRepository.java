package ru.afonov.colorpalette.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.afonov.colorpalette.model.Color;

public interface ColorRepository extends JpaRepository<Color, Long> {

}
