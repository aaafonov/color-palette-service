package ru.afonov.colorpalette.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Color {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String hex;

    private String name;

    @ManyToOne
    @JoinColumn(name = "palette_id")
    private Palette palette;
}
