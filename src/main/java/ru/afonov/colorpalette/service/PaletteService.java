package ru.afonov.colorpalette.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.afonov.colorpalette.dto.PaletteDTO;
import ru.afonov.colorpalette.dto.PaletteRequest;
import ru.afonov.colorpalette.dto.mapper.PaletteMapper;
import ru.afonov.colorpalette.model.Color;
import ru.afonov.colorpalette.model.Palette;
import ru.afonov.colorpalette.model.User;
import ru.afonov.colorpalette.repository.PaletteRepository;
import ru.afonov.colorpalette.security.CustomUserDetails;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Класс для работы с палитрами
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class PaletteService {
    private final PaletteRepository paletteRepository;
    private final UserService userService;
    private final PaletteMapper paletteMapper;

    /**
     * Метод для получения всех палитр у аутентифицированного пользователя
     *
     * @return Список с данными о палитрах
     */
    public List<PaletteDTO> getPalettes() {
        List<Palette> palettes = paletteRepository.findByUserId(getCurrentUserId());
        log.info("Получены палитры: {}", palettes);

        return palettes.stream()
                .map(paletteMapper::mapToDTO)
                .toList();
    }

    private long getCurrentUserId() {
        CustomUserDetails currentUserDetails = userService.getCurrentUserDetails();
        return currentUserDetails.getId();
    }

    /**
     * Метод для получения палитры по ее идентификатору у аутентифицированного пользователя
     *
     * @param id идентификатор палитры
     * @return Данны о палитре
     */
    public PaletteDTO getPalette(Long id) {
        Palette palette = getPaletteByIdForCurrentUser(id);
        return paletteMapper.mapToDTO(palette);
    }

    private Palette getPaletteByIdForCurrentUser(long paletteId) {
        CustomUserDetails currentUserDetails = userService.getCurrentUserDetails();

        Palette palette = paletteRepository.findByIdAndUserId(paletteId, currentUserDetails.getId())
                .orElseThrow(() ->
                        new EntityNotFoundException(String.format("У пользователя с логином %s не найдено палитры с идентификатором %s",
                                currentUserDetails.getUsername(), paletteId)));
        log.info("Получена палитра: {}", palette);
        return palette;
    }

    /**
     * Метод для создания палитры
     *
     * @param paletteDTO данные палитры
     * @return Данные сохраненной палитры
     */
    public PaletteDTO createPalette(PaletteRequest paletteDTO) {
        CustomUserDetails currentUserDetails = userService.getCurrentUserDetails();
        User user = userService.findUserByLogin(currentUserDetails.getUsername());
        Palette palette = paletteMapper.mapToEntity(paletteDTO);
        palette.setUser(user);
        paletteRepository.save(palette);
        log.info("Создана палитра: {}", palette);

        return paletteMapper.mapToDTO(palette);
    }

    /**
     * Метод для обновления палитры
     *
     * @param id         идентификатор палитры
     * @param paletteDTO данные для обновления палитры
     * @return Данные обновленной палитры
     */
    public PaletteDTO updatePalette(Long id, PaletteRequest paletteDTO) {
        log.info("Обновление палитры с идентификатором: {}", id);
        Palette palette = getPaletteByIdForCurrentUser(id);

        palette.setName(paletteDTO.getName());
        Palette savedPalette = paletteRepository.save(palette);
        log.info("Обновленная палитра: {}", savedPalette);

        return paletteMapper.mapToDTO(savedPalette);
    }

    /**
     * Метод для удаления палитры
     *
     * @param id идентификатор палитры
     */
    public void deletePalette(Long id) {
        Palette palette = getPaletteByIdForCurrentUser(id);
        paletteRepository.delete(palette);

        log.info("Удалена палитра: {}", palette);
    }

    /**
     * Метод для получения палитры из БД по ее идентификатору
     *
     * @param id идентификатор палитры
     * @return Сущность палитры
     */
    public Palette getPaletteEntityById(Long id) {
        return getPaletteByIdForCurrentUser(id);
    }

    /**
     * Метод для получения данных о цвете палитры
     *
     * @param colorId   идентификатор цвета
     * @param paletteId идентификатор палитры
     * @return Данные о цвете
     */
    public Color getColorFromPalette(Long colorId, Long paletteId) {
        Palette palette = getPaletteByIdForCurrentUser(paletteId);
        return palette.getColors().stream()
                .filter(color -> color.getId().equals(colorId))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Не найдено цвета с идентификатором %s у палитры %s",
                                colorId, palette.getName())));
    }
}
