package ru.afonov.colorpalette.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.afonov.colorpalette.dto.AuthResponse;
import ru.afonov.colorpalette.dto.LoginRequest;
import ru.afonov.colorpalette.dto.UserDTO;
import ru.afonov.colorpalette.dto.mapper.UserMapper;
import ru.afonov.colorpalette.exception.ColorPaletteException;
import ru.afonov.colorpalette.model.User;
import ru.afonov.colorpalette.repository.UserRepository;
import ru.afonov.colorpalette.security.CustomUserDetails;

import javax.persistence.EntityNotFoundException;

/**
 * Класс для работы с пользователями
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;

    /**
     * Метод для создания пользователя
     *
     * @param userDTO данные пользователя
     */
    public void createUser(UserDTO userDTO) {
        validateUserDoesNotExist(userDTO.getLogin());

        User user = userMapper.mapToEntity(userDTO);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        log.info("Создан пользователь с логином: {}", user.getLogin());
    }

    private void validateUserDoesNotExist(String login) {
        userRepository.findByLogin(login)
                .ifPresent(user -> {
                    throw new ColorPaletteException(String.format("Пользователь с логином %s уже существует", user.getLogin()));
                });
    }

    /**
     * Метод для поиска пользователя по его логину
     *
     * @param login логин пользователя
     * @return Данные зарегистрированного пользователя
     */
    public User findUserByLogin(String login) {
        return userRepository.findByLogin(login)
                .orElseThrow(() ->
                        new EntityNotFoundException(String.format("Пользователь с логином %s не найден", login)));
    }

    /**
     * Метод для аутентификации пользователя
     *
     * @param request реквизиты для аутентификации
     * @return Объект с токеном доступа
     */
    public AuthResponse signIn(LoginRequest request) {
        Authentication authentication = authenticateUser(request.getLogin(), request.getPassword());

        if (authentication.isAuthenticated()) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(request.getLogin());
            String token = jwtService.generateToken(userDetails);

            log.info("Пользователь с логином {} аутентифицирован", request.getLogin());
            return new AuthResponse(token);
        } else {
            throw new BadCredentialsException("Пользователь не аутентифицирован");
        }
    }

    private Authentication authenticateUser(String login, String password) {
        return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login, password)
        );
    }

    /**
     * Метод для получения данных аутентифицированного пользователя
     *
     * @return Данные пользователя
     */
    public CustomUserDetails getCurrentUserDetails() {
        return (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
    }
}
