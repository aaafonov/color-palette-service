package ru.afonov.colorpalette.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.afonov.colorpalette.client.ColorClient;
import ru.afonov.colorpalette.client.ColorDetails;
import ru.afonov.colorpalette.dto.ColorDTO;
import ru.afonov.colorpalette.dto.ColorRequest;
import ru.afonov.colorpalette.dto.mapper.ColorMapper;
import ru.afonov.colorpalette.model.Color;
import ru.afonov.colorpalette.model.Palette;
import ru.afonov.colorpalette.repository.ColorRepository;

import java.util.List;

/**
 * Сервис для работы с цветами
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ColorService {

    private final ColorRepository colorRepository;
    private final PaletteService paletteService;
    private final ColorMapper colorMapper;
    private final ColorClient colorClient;

    /**
     * Метод для получения списка цветов по идентификатору палитры
     *
     * @param paletteId идентификатор палитры
     * @return Список объектов с описанием цветов
     */
    @Transactional(readOnly = true)
    public List<ColorDTO> getColors(Long paletteId) {
        Palette palette = paletteService.getPaletteEntityById(paletteId);
        List<Color> paletteColors = palette.getColors();
        log.info("Получены цвета для палитры: {}", paletteColors);

        return paletteColors.stream()
                .map(colorMapper::mapToDTO)
                .toList();
    }

    /**
     * Метод для получения информации о цвете
     *
     * @param paletteId идентификатор палитры
     * @param colorId   идентификатор цвета
     * @return Данные о цвете
     */
    @Transactional(readOnly = true)
    public ColorDTO getColor(Long paletteId, Long colorId) {
        Color color = paletteService.getColorFromPalette(colorId, paletteId);
        log.info("Получен цвет: {}", color);

        return colorMapper.mapToDTO(color);
    }

    /**
     * Метод для создания цвета
     *
     * @param paletteId идентификатор палитры
     * @param colorDTO  данные цвета
     * @return Сохраненные данные о цвете
     */
    @Transactional
    public ColorDTO createColor(Long paletteId, ColorRequest colorDTO) {
        Palette palette = paletteService.getPaletteEntityById(paletteId);
        ColorDetails colorDetails = colorClient.getColorInfo(colorDTO.getHex());
        log.info("Получены данные для цвета: {}", colorDetails);

        Color color = colorMapper.mapToEntity(colorDTO);
        color.setName(colorDetails.getColorName());
        color.setPalette(palette);
        log.info("Создан цвет: {}", color);

        return colorMapper.mapToDTO(colorRepository.save(color));
    }

    /**
     * Метод для обновления данных о цвете
     *
     * @param paletteId       идентификатор палитры
     * @param colorId         идентификатор цвета
     * @param updatedColorDTO данные для обновления цвета
     * @return Обновленные данные о цвете
     */
    @Transactional
    public ColorDTO updateColor(Long paletteId, Long colorId, ColorRequest updatedColorDTO) {
        log.info("Обновление цвета с идентификатором: {}", colorId);
        
        ColorDetails colorDetails = colorClient.getColorInfo(updatedColorDTO.getHex());
        log.info("Получены обновленные данные для цвета: {}", colorDetails);

        Color color = paletteService.getColorFromPalette(colorId, paletteId);
        color.setHex(updatedColorDTO.getHex());
        color.setName(colorDetails.getColorName());
        log.info("Обновленные данные цвета: {}", color);

        return colorMapper.mapToDTO(colorRepository.save(color));
    }

    /**
     * Метод для удаления цвета
     *
     * @param paletteId идентификатор палитры
     * @param colorId   идентификатор цвета
     */
    @Transactional
    public void deleteColor(Long paletteId, Long colorId) {
        Color color = paletteService.getColorFromPalette(colorId, paletteId);
        colorRepository.delete(color);

        log.info("Удален цвет: {}", color);
    }
}
