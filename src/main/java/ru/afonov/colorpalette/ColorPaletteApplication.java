package ru.afonov.colorpalette;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@OpenAPIDefinition(info = @Info(title = "PALETTE SERVICE API", version = "v1"))
@SecurityScheme(
		name = "jwt",
		type = SecuritySchemeType.HTTP,
		bearerFormat = "JWT",
		scheme = "bearer"
)
@EnableFeignClients
@SpringBootApplication
public class ColorPaletteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColorPaletteApplication.class, args);
	}

}
