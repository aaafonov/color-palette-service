package ru.afonov.colorpalette.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.afonov.colorpalette.model.User;
import ru.afonov.colorpalette.repository.UserRepository;

import java.util.ArrayList;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Пользователь с логином %s не найден", username)));

        return new CustomUserDetails(
                user.getId(),
                user.getName(),
                user.getLogin(),
                user.getPassword(),
                new ArrayList<>()
        );
    }
}
