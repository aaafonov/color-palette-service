package ru.afonov.colorpalette.exception;

public class ColorPaletteException extends RuntimeException {

    public ColorPaletteException(String e) {
        super(e);
    }
}
