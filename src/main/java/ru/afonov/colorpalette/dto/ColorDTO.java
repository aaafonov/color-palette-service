package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Данные о цвете")
public class ColorDTO {

    @Schema(description = "Идентификатор цвета", example = "1")
    private Long id;

    @Schema(description = "Hex код цвета", example = "152EEA")
    private String hex;

    @Schema(description = "Название цвета", example = "Cobalt")
    private String name;
}
