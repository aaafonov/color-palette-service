package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Информация о цветовой палитре")
public class PaletteDTO {

    @Schema(description = "Идентификатор палитры", example = "1")
    private Long id;

    @Schema(description = "Имя палитры", example = "Моя палитра")
    private String name;
}
