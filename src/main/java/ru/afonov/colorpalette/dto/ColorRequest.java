package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Запрос на изменение цвета")
public class ColorRequest {

    @Schema(description = "Hex код цвета", example = "152EEA")
    private String hex;
}
