package ru.afonov.colorpalette.dto.mapper;

import org.mapstruct.Mapper;
import ru.afonov.colorpalette.dto.ColorDTO;
import ru.afonov.colorpalette.dto.ColorRequest;
import ru.afonov.colorpalette.model.Color;

@Mapper
public interface ColorMapper {
    ColorDTO mapToDTO(Color color);

    Color mapToEntity(ColorRequest colorRequest);
}
