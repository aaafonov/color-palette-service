package ru.afonov.colorpalette.dto.mapper;

import org.mapstruct.Mapper;
import ru.afonov.colorpalette.dto.PaletteDTO;
import ru.afonov.colorpalette.dto.PaletteRequest;
import ru.afonov.colorpalette.model.Palette;

@Mapper
public interface PaletteMapper {
    PaletteDTO mapToDTO(Palette palette);

    Palette mapToEntity(PaletteRequest paletteRequest);
}
