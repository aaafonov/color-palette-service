package ru.afonov.colorpalette.dto.mapper;

import org.mapstruct.Mapper;
import ru.afonov.colorpalette.dto.UserDTO;
import ru.afonov.colorpalette.model.User;

@Mapper
public interface UserMapper {
    User mapToEntity(UserDTO userDTO);
}
