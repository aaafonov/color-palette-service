package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Ответ с ошибкой")
public class ErrorResponse {

    @Schema(description = "Идентификатор ошибки", example = "662fc1f1-fcac-4e53-984d-fd160aa81354")
    private UUID id;

    @Schema(description = "Информация об ошибке", example = "Цвет не найден в БД")
    private String message;
}
