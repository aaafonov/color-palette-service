package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Запрос на изменение цветовой палитры")
public class PaletteRequest {

    @Schema(description = "Имя палитры", example = "Моя палитра")
    private String name;
}
