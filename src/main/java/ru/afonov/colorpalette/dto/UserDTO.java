package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Данные о пользователе")
public class UserDTO {

    @Schema(description = "Имя пользователя", example = "Иван")
    private String name;

    @Schema(description = "Логин пользователя", example = "ivan123")
    private String login;

    @Schema(description = "Пароль пользователя", example = "password")
    private String password;
}
