package ru.afonov.colorpalette.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Запрос на аутентификацию пользователя")
public class LoginRequest {

    @Schema(description = "Логин пользователя", example = "login")
    private String login;

    @Schema(description = "Пароль пользователя", example = "password")
    private String password;
}
