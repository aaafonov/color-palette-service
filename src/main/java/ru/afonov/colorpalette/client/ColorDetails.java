package ru.afonov.colorpalette.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Данные о цвете
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColorDetails {

    /**
     * Данные об имени цвета
     */
    private ColorName name;

    public String getColorName() {
        return name.getValue();
    }
}
