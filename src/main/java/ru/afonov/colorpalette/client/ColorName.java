package ru.afonov.colorpalette.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Данные о названии цвета
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ColorName {

    /**
     * Значение цвета
     */
    private String value;

    /**
     * Ближайший hex код цвета
     */
    @JsonProperty("closest_named_hex")
    private String closestNamedHex;

    /**
     * Признак точного соответствия
     */
    @JsonProperty("exact_match_name")
    private boolean exactMatchName;

    /**
     * Расстояние до точного соответствия
     */
    private int distance;
}
