package ru.afonov.colorpalette.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Клиент для получения данных о цвете
 */
@FeignClient(name = "colorClient", url = "https://www.thecolorapi.com")
public interface ColorClient {

    /**
     * Запрос на получение информации для одного цвета
     *
     * @param hexCode код цвета
     * @return данные о цвете
     */
    @GetMapping("/id")
    ColorDetails getColorInfo(@RequestParam("hex") String hexCode);
}
