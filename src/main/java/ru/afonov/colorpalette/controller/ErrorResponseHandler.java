package ru.afonov.colorpalette.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.afonov.colorpalette.dto.ErrorResponse;
import ru.afonov.colorpalette.exception.ColorPaletteException;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Slf4j
@RestControllerAdvice
public class ErrorResponseHandler {

    @ExceptionHandler({EntityNotFoundException.class, ColorPaletteException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(RuntimeException exception) {
        log.error("Произошла ошибка: {}", exception.getMessage());

        return ErrorResponse.builder()
                .id(UUID.randomUUID())
                .message(exception.getMessage())
                .build();
    }
}
