package ru.afonov.colorpalette.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.afonov.colorpalette.dto.ColorDTO;
import ru.afonov.colorpalette.dto.ColorRequest;
import ru.afonov.colorpalette.service.ColorService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
@RequestMapping("/palettes/{paletteId}/colors")
@Tag(name = "API для работы с цветами")
public class ColorController {

    private final ColorService colorService;

    @Operation(summary = "Получение коллекции цветов по идентификатору палитры")
    @GetMapping
    public ResponseEntity<List<ColorDTO>> getColors(@PathVariable Long paletteId) {
        return ResponseEntity.ok(colorService.getColors(paletteId));
    }

    @Operation(summary = "Получение цвета по идентификатору")
    @GetMapping("/{colorId}")
    public ResponseEntity<ColorDTO> getColor(@PathVariable Long paletteId, @PathVariable Long colorId) {
        return ResponseEntity.ok(colorService.getColor(paletteId, colorId));
    }

    @Operation(summary = "Создание цвета")
    @PostMapping
    public ResponseEntity<ColorDTO> createColor(@PathVariable Long paletteId, @RequestBody ColorRequest colorRequest) {
        return ResponseEntity.ok(colorService.createColor(paletteId, colorRequest));
    }

    @Operation(summary = "Изменение цвета")
    @PutMapping("/{colorId}")
    public ResponseEntity<ColorDTO> updateColor(@PathVariable Long paletteId, @PathVariable Long colorId,
                                                @RequestBody ColorRequest colorRequest) {
        return ResponseEntity.ok(colorService.updateColor(paletteId, colorId, colorRequest));
    }

    @Operation(summary = "Удаление цвета")
    @DeleteMapping("/{colorId}")
    public ResponseEntity<Void> deleteColor(@PathVariable Long paletteId, @PathVariable Long colorId) {
        colorService.deleteColor(paletteId, colorId);
        return ResponseEntity.ok(null);
    }
}
