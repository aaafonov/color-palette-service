package ru.afonov.colorpalette.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.afonov.colorpalette.dto.PaletteDTO;
import ru.afonov.colorpalette.dto.PaletteRequest;
import ru.afonov.colorpalette.service.PaletteService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
@RequestMapping("/palettes")
@Tag(name = "API для работы с палитрами")
public class PaletteController {

    private final PaletteService paletteService;

    @Operation(summary = "Получение коллекции палитр")
    @GetMapping
    public ResponseEntity<List<PaletteDTO>> getPalettes() {
        return ResponseEntity.ok(paletteService.getPalettes());
    }

    @Operation(summary = "Получение палитры по идентификатору")
    @GetMapping("/{id}")
    public ResponseEntity<PaletteDTO> getPalette(@PathVariable Long id) {
        return ResponseEntity.ok(paletteService.getPalette(id));
    }

    @Operation(summary = "Создание палитры")
    @PostMapping
    public ResponseEntity<PaletteDTO> createPalette(@RequestBody PaletteRequest paletteRequest) {
        return ResponseEntity.ok(paletteService.createPalette(paletteRequest));
    }

    @Operation(summary = "Изменение палитры")
    @PutMapping("/{id}")
    public ResponseEntity<PaletteDTO> updatePalette(@PathVariable Long id, @RequestBody PaletteRequest paletteRequest) {
        return ResponseEntity.ok(paletteService.updatePalette(id, paletteRequest));
    }

    @Operation(summary = "Удаление палитры")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePalette(@PathVariable Long id) {
        paletteService.deletePalette(id);
        return ResponseEntity.ok(null);
    }
}
