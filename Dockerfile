# Используем Alpine-based OpenJDK 17 образ для базы
FROM openjdk:17-jdk-alpine AS build

# Установка Maven
RUN apk add --no-cache maven

# Копируем исходники в Docker образ
COPY src /home/app/src
COPY pom.xml /home/app

# Сборка проекта
RUN mvn -f /home/app/pom.xml clean package -Dmaven.test.skip

# Запускаем .jar файл на базе образа openjdk
FROM openjdk:17-jdk-alpine AS runtime

# Копирование .jar файла из сборочного образа
COPY --from=build /home/app/target/*.jar app.jar

# Запуск приложения
ENTRYPOINT ["java", "-jar","/app.jar"]